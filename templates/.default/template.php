<?php


if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$this->setFrameMode(true);
?>
<?if (strlen($arResult['PREVIEW_TEXT']) > 0):?>
    <div class="seotext preview"><?=$arResult['PREVIEW_TEXT']; ?></div>
<?endif;?>
<?if (strlen($arResult['MAIN_TEXT']) > 0):?>
    <div class="seotext"><?=$arResult['MAIN_TEXT']; ?></div>
<?endif;?>
