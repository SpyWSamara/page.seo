<?php

$MESS['IBLOCK_MODULE_NOT_LOADED'] = 'Не удалось подключить модуль инфоблоков';
$MESS['REL_PAGE_SEO_IBLOCK_ID_NAME'] = 'ID инфоблока';
$MESS['REL_PAGE_SEO_ELEMENT_ID_NAME'] = 'Элемент содержащий SEO птимизацию';
$MESS['REL_PAGE_SEO_ELEMENT_ID_SEARCH_PLACEHOLDER'] = 'Поиск элемента';
$MESS['REL_PAGE_SEO_ELEMENT_ID_SELECTED_PLACEHOLDER'] = 'Элемент не выбран';
