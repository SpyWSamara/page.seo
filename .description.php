<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
   'NAME' => Loc::getMessage('REL_PAGE_SEO_NAME'),
   'DESCRIPTION' => Loc::getMessage('REL_PAGE_SEO_DESCRIPTION'),
   'PATH' => array(
      'ID' => 'relevant',
      'NAME' => Loc::getMessage('REL_COMPONENTS'),
   ),
   'CACHE_PATH' => 'Y',
);
