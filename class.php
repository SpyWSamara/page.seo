<?php

namespace Relevant\Components;

use Bitrix\Main;
use Bitrix\Iblock;

if (!Main\Loader::includeModule('iblock')) {
    return ShowError('IBLOCK_MODULE_NOT_LOADED');
}

class PageSeo extends \CBitrixComponent
{
    protected $iblockId;
    protected $elementId;
    protected $selectFields = [
        'ID',
        'NAME',
        'PREVIEW_TEXT',
        'PREVIEW_TEXT_TYPE',
        'MAIN_TEXT' => 'DETAIL_TEXT',
        'DETAIL_TEXT_TYPE',
    ];

    protected $element;

    public function onPrepareComponentParams(array $params)
    {
        $this->iblockId = (int) $params['IBLOCK_ID'];
        $this->elementId = (int) $params['ELEMENT_ID'];

        return parent::onPrepareComponentParams($params);
    }

    public function executeComponent()
    {
        if ($this->startResultCache()) {
            if ($this->getElement()) {
                $this->getElementSeo();
            }
            $this->registerCacheTag('iblock_id_'.$this->iblockId);
            $this->registerCacheTag('iblock_element_id_'.$this->elementId);
            $this->includeComponentTemplate();
        }
        $this->setMeta();
    }

    protected function getElement()
    {
        $this->arResult = Iblock\ElementTable::getRow([
            'filter' => ['=IBLOCK_ID' => $this->iblockId, '=ACTIVE' => 'Y', '=ID' => $this->elementId],
            'select' => $this->selectFields,
        ]);

        if (!empty($this->arResult['PREVIEW_TEXT']) && 'text' == $this->arResult['PREVIEW_TEXT_TYPE']) {
            $this->arResult['PREVIEW_TEXT'] = htmlspecialchars($this->arResult['PREVIEW_TEXT']);
        }
        if (!empty($this->arResult['MAIN_TEXT']) && 'text' == $this->arResult['DETAIL_TEXT_TYPE']) {
            $this->arResult['MAIN_TEXT'] = htmlspecialchars($this->arResult['MAIN_TEXT']);
        }

        return !empty($this->arResult);
    }

    protected function getElementSeo()
    {
        $ipropValues = new Iblock\InheritedProperty\ElementValues($this->iblockId, $this->elementId);
        $this->arResult += $ipropValues->getValues();
    }

    protected function getTaggedCache()
    {
        return Main\Application::getInstance()->getTaggedCache();
    }

    protected function registerCacheTag($key)
    {
        if (!empty($key)) {
            $this->getTaggedCache()->registerTag($key);
        }
    }

    protected function setMeta()
    {
        if (!empty($this->arResult['ELEMENT_PAGE_TITLE'])) {
            $this->getLegacyApplication()->setTitle($this->arResult['ELEMENT_PAGE_TITLE']);
        }
        if (!empty($this->arResult['ELEMENT_META_TITLE'])) {
            $this->getLegacyApplication()->setPageProperty('title', $this->arResult['ELEMENT_META_TITLE']);
        }
        if (!empty($this->arResult['ELEMENT_META_KEYWORDS'])) {
            $this->getLegacyApplication()->setPageProperty('keywords', $this->arResult['ELEMENT_META_KEYWORDS']);
        }
        if (!empty($this->arResult['ELEMENT_META_DESCRIPTION'])) {
            $this->getLegacyApplication()->setPageProperty('description', $this->arResult['ELEMENT_META_DESCRIPTION']);
        }
    }

    protected function getLegacyApplication()
    {
        return $GLOBALS['APPLICATION'];
    }
}
