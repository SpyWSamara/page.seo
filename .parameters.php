<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock')) {
    return ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_LOADED'));
}

$groups = [];
$parameters = [
    'CACHE_TIME' => ['DEFAULT' => 36000000],
];

$iblocks = \Bitrix\Iblock\IblockTable::getList([
    'filter' => ['=IBLOCK_TYPE_ID' => 'seo_iblocks', '=ACTIVE' => 'Y'],
    'select' => ['ID', 'NAME'],
])->fetchAll();
$parameters['IBLOCK_ID'] = [
    'PARENT' => 'DATA_SOURCE',
    'NAME' => Loc::getMessage('REL_PAGE_SEO_IBLOCK_ID_NAME'),
    'TYPE' => 'LIST',
    'VALUES' => [],
    'REFRESH' => 'Y',
];
foreach ($iblocks as $iblock) {
    $parameters['IBLOCK_ID']['VALUES'][$iblock['ID']] = '['.$iblock['ID'].'] '.$iblock['NAME'];
}

if (!$arCurrentValues['IBLOCK_ID']) {
    reset($parameters['IBLOCK_ID']['VALUES']);
    $arCurrentValues['IBLOCK_ID'] = key($parameters['IBLOCK_ID']['VALUES']);
}

$elements = \Bitrix\Iblock\ElementTable::getList([
    'filter' => ['=IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], '=ACTIVE' => 'Y'],
    'select' => ['ID', 'NAME'],
])->fetchAll();

$parameters['ELEMENT_ID'] = [
    'PARENT' => 'DATA_SOURCE',
    'NAME' => Loc::getMessage('REL_PAGE_SEO_ELEMENT_ID_NAME'),
    'TYPE' => 'CUSTOM',
    'JS_FILE' => $componentPath.'/settings/elementid.js',
    'JS_EVENT' => 'oninit',
    'JS_DATA' => [
        'elements' => $elements,
        'current' => $arCurrentValues['ELEMENT_ID'],
        'searchPlaceholder' => Loc::getMessage('REL_PAGE_SEO_ELEMENT_ID_SEARCH_PLACEHOLDER'),
        'selectedPlaceholder' => Loc::getMessage('REL_PAGE_SEO_ELEMENT_ID_SELECTED_PLACEHOLDER'),
    ],
    'DEFAULT' => 0,
];

$arComponentParameters = [
   'GROUPS' => $groups,
   'PARAMETERS' => $parameters,
];
