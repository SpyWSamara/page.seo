function oninit(params) {
    var selectElement = function(event) {
        currentElement.innerText = this.options[this.options.selectedIndex].text;
        params.oInput.value = this.value;
    }
    var filterElements = function(event) {
        var options = selectList.querySelectorAll('option'),
            filter = this.value;

        for (var index in options) {
            if (options.hasOwnProperty(index)) {
                var option = options[index],
                    display = option.text.toLocaleLowerCase().indexOf(filter) === -1 ? 'none' : '';

                BX.style(option, 'display', display);
            }
        }
    }

    var searchInput = BX.create('input', {
        'props': {
            'placeholder': params.data.searchPlaceholder
        },
        'attrs': {
            'type': 'text'
        },
        'events': {
            'keyup': filterElements
        }
    });

    BX.append(searchInput, params.oCont);

    var currentElement = BX.create('p', {
        'text': params.data.selectedPlaceholder
    });

    BX.append(currentElement, params.oCont);

    var selectList = BX.create('select', {
        'attrs': {
            'size': 5
        },
        'events': {
            'change': selectElement
        }
    });

    BX.append(selectList, params.oCont);

    for (var index in params.data.elements) {
        if (params.data.elements.hasOwnProperty(index)) {
            var element = params.data.elements[index],
                option = BX.create('option', {
                    'attrs': {
                        'value': element.ID,
                        'selected': element.ID == params.data.current,
                    },
                    'text': '[' + element.ID + '] ' + element.NAME,
                });

            BX.append(option, selectList);
            if (element.ID == params.data.current) {
                currentElement.innerText = option.innerText;
            }
        }
    }
}
